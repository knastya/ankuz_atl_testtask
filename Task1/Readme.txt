Task1
A team of developers is inexperienced with writing automated tests and have asked for your help to get them started. The target of the testing
is Confluence Cloud, Atlassian’s hosted wiki product.
To help them, write some example automated tests to verify that a user can:
• Create a new page.
• Set restrictions on an existing page.

1.Project contains 

-conf_tests\pages\: implementation of different type of Web Pages (hierarchical page tree).  
Page object contains elements (locators) unique for this page type. All other web pages should be added there for example, Settings page, Logout page etc)

-conf_tests\model\: contains objects that modelling different objects used for correct application work. 
Contains Application model object that interacts with Web pages and hides the details from the tests level. 

-conf_tests\conftest.py: project configuration file. Contains 'session'-level fixtures and command string parser .

-conf_tests\knastya_tests_file.py: main file with tests. Contains 3 tests: login, create page, set restriction.

-conf_tests\ENV\: virtual environment for test running


2.Improvements that should be done for real life tests

2.1. Logging implemented
2.2. Check with different browser (tests were completed on GoogleChrome)
2.3. More detailed comments
2.5. More detailed test project description (kind of user manual for using and extend it)
Tests:  
2.4. Logout test
2.5. Remove page test (the good idea is to leave the application state like before test performed)
2.6. Check of changing any restrictions, not only from "none" to "viewedit"
2.7. Check and  process the situation when page with title given is already exists
2.8. Other negative tests 