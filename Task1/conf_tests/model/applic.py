##Created by Kuznetsova Anastasia 29.08.16

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import *
from pages.page_page import Page
from pages.internal_page import InternalPage
from selenium.webdriver.support.select import Select
from pages.login_page import LoginPage
from pages.confl_page import ConfluencePage
from pages.editpage_page import EditingPage
from pages.userpage_page import UserPage
from selenium.webdriver.common.keys import Keys
import datetime


##### This class contains the main info about application, main action with pages###

class Application(object):

    def __init__(self, driver, wait,base_url):
        self.driver = driver
        self.wait = wait
        self.base_url = base_url
        self.login_page = LoginPage(driver, base_url)
        self.internal_page = InternalPage (driver, base_url)
        self.confl_page = ConfluencePage(driver, base_url)
        self.editing_page = EditingPage(driver, base_url)
        self.page1_page = UserPage(driver, base_url, '')

#--------- Go to URL -----------------------

#   open application homepage
    def go_to_home_page(self):
        self.driver.get(self.base_url)

#   open main Confuence page
    def go_to_confluence(self):
        self.driver.get(self.base_url+'wiki/all-updates')

#   open any confluence page with title given
    def go_to_userpage(self,page_title):
        self.driver.get(self.base_url+'wiki/display/BP/'+page_title)

#----------- Login and Logout -------------------

#   Login with give users credentials
    def do_login(self, user):
        lp = self.login_page
        lp.username_field.clear()
        lp.username_field.send_keys(user.username)
        lp.password_field.clear()
        lp.password_field.send_keys(user.password)
        lp.submit_button.click()

#   Check if logged in
    def is_logged_in(self):
        return self.internal_page.is_this_page


#    Logout functions should be implemented
#    def do_logout(self):
#    def is_not_logged_in(self):


#---------- Create new page -----------

#   Check if current page is new creating page
    def is_editing_page(self):
        return self.editing_page.is_this_page

#   Create new page button click
    def click_createpage_btn(self):
        self.confl_page.create_button.click()

#   Enter title given to the title page and submit page creating
    def fill_pagetitle_and_submit(self, newtitle):
        ep=self.editing_page
        ep.title_field.clear()
        ep.title_field.send_keys(newtitle)
        ep.submit_btn.click()

#   Check if current page is users page with tittle given
    def is_userpage_open(self,title):
        return self.page1_page.is_this_page_with_title(title)



#------------ Set restrictions -------------------

#   Click restriction button
    def click_restrict_page_btn(self):
        self.page1_page.restrict_button.click()

#   Check if current page restrictions is "viewedit" -the strongest one
    def check_viewedit_restrict(self):
        return self.page1_page.is_viewedit_restrict()

#   Select "viewedit" restriction from the dropdown list
    def do_select_viewedit_restrict(self):
        pp=self.page1_page
        pp.restrict_dropdown.click()
        dropdown=Select(pp.restrict_dropdown_open)
        dropdown.select_by_value("viewedit")
        pp.submit_restrict_button.click()



#---------- Utilites ------------------

#   Operate with alerts popup
    def confirm_alert(self):
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            alert.accept()

#   Create unique title for a new page based on current timestamp
    def get_new_page_title(self):
        return ("test_page" + str(datetime.datetime.now()))

