##Created by Kuznetsova Anastasia 29.08.16

## The User datas
class User(object):

    def __init__(self, username="", password="", email=""):
        self.username = username
        self.password = password
        self.email = email

    @classmethod
    def Admin(cls):
        return cls(username="nastyakuz@front.ru", password="qwe123")

    @classmethod
    def random(cls):
        from random import randint
        return cls(username="user" + str(randint(0, 1000000)), password="pass" + str(randint(0, 1000000)))
