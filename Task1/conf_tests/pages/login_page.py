##Created by Kuznetsova Anastasia 29.08.16

from page_page import Page
from selenium.webdriver.common.by import By

# This is Login page

class LoginPage(Page):


#   Username field
    @property
    def username_field(self):
        return self.driver.find_element_by_id("username")

#   Password field
    @property
    def password_field(self):
        return self.driver.find_element_by_name("password")

#   LogIn button
    @property
    def submit_button(self):
        return self.driver.find_element_by_id("login")


#   Is Login button present
    @property
    def is_this_page(self):
        return self.is_element_visible((By.ID, "login"))

