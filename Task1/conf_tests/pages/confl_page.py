##Created by Kuznetsova Anastasia 29.08.16

from internal_page import InternalPage
from selenium.webdriver.common.by import By

#This is any confluence page (not JIRA)

class ConfluencePage(InternalPage):

#   Create new page button
    @property
    def create_button(self):
        return self.driver.find_element_by_id("quick-create-page-button")

#   Is current page is Confluence page
    @property
    def is_this_page(self):
        return self.is_element_visible((By.ID,"Confluence"))

