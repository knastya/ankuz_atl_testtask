##Created by Kuznetsova Anastasia 29.08.16

# -*- coding: utf-8 -*-


from conftest import app
from model.user import User
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.expected_conditions import *
import pytest
from selenium import webdriver
from model.applic import Application
import time


#------- Functions used in test -------------------

#       Login to base page (https://knastya.atlassian.net/)
def do_login(app):
        app.go_to_home_page()
        app.do_login(User.Admin())
        assert app.is_logged_in()

#       Creates new page with title given
#       Precondition: User is logged in, page with title given not exists
def do_create_page(app,page_title):
        app.go_to_confluence()
        app.click_createpage_btn()
        assert app.is_editing_page()
        app.fill_pagetitle_and_submit(page_title)
        assert app.is_userpage_open(page_title)

#       Change restriction to "viewedit" for existent page with title given
#       Precondition: User is logged in, page with title given exists, the current restriction is not "viewedit"
def do_set_viewedit_restr(app,page_title):
        app.go_to_userpage(page_title)
        app.click_restrict_page_btn()
        app.do_select_viewedit_restrict()
        assert app.check_viewedit_restrict()


#============= TESTS ===============================

#       Login from the very beginning with Admin credentials
def test_login(app):
        do_login(app)


#should be implemented
# def test_logout(app):

#       Creates new page with automatically generated title
#       Check that page has been created
def test_create_page(app):
        do_login(app)
        newtitle = app.get_new_page_title(app)
        do_create_page(app,newtitle)

# should be implemented
# def test_remove_page(app):

#       Change current restrictions to the "viewedit" -the strongest one
#       Check the restrictions
def test_set_editing_restrict(app):
        do_login(app)
        newtitle = app.get_new_page_title()
        do_create_page(app, newtitle)
        do_set_viewedit_restr(app,newtitle)






