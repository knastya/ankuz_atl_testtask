##Created by Kuznetsova Anastasia 29.08.16

from confl_page import ConfluencePage
from selenium.webdriver.common.by import By

# Any confluence page created by user
# This page has tittle

class UserPage(ConfluencePage):

    def __init__(self, driver, base_url, title):
        ConfluencePage.__init__(self, driver, base_url)
        self.title = title

#   Set restrictions button
    @property
    def restrict_button(self):
        return self.driver.find_element_by_id("content-metadata-page-restrictions")

#   Is this page created bu user
    @property
    def is_this_page(self):
        return self.is_element_visible((By.ID, "editPageLink"))

#   Is this page has title given
    def is_this_page_with_title(self, title):
        return self.is_element_visible((By.LINK_TEXT, title))

#   Restrict dropdown button in the Restriction dialog
    @property
    def restrict_dropdown(self):
        return self.is_element_visible((By.ID, "s2id_page-restrictions-dialog-selector"))

#   Restrict dropdown list in the Restriction dialog
    @property
    def restrict_dropdown_open(self):
        return self.is_element_visible((By.ID, "page-restrictions-dialog-selector"))

#   Restrict submitting button in the Restriction dialog
    @property
    def submit_restrict_button(self):
        return self.driver.find_element_by_id("page-restrictions-dialog-save-button")

#   Is the current restriction is "viewedit"
    def is_viewedit_restrict(self):
        if self.is_element_visible((By.CSS_SELECTOR, "a.aui-icon-small.aui-iconfont-locked.restricted")):
            return True
        else:
            return False

