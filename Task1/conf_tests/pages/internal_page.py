##Created by Kuznetsova Anastasia 29.08.16

from page_page import Page
from selenium.webdriver.common.by import By


#This is any internal (logged in) page: either JIRA or Confluence

class InternalPage(Page):

#   For logged in user
    @property
    def is_this_page(self):
        return self.is_element_visible((By.ID, "header-details-user-fullname"))


