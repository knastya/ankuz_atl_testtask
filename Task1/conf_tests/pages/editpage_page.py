##Created by Kuznetsova Anastasia 29.08.16

from confl_page import ConfluencePage
from selenium.webdriver.common.by import By

#This is template of new page when user click CreatePage

class EditingPage(ConfluencePage):

#   Is it user page with empty title
    @property
    def is_this_page(self):
        return ( self.driver.find_element_by_id("content-title").get_attribute("value")=='')

#   Page tittle
    @property
    def title_field(self):
        return self.driver.find_element_by_id("content-title")

#   Submit creating page button
    @property
    def submit_btn(self):
        return self.driver.find_element_by_id("rte-button-publish")